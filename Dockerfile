FROM centos:7

RUN yum install -y make wget gcc openssl-devel bzip2-devel libffi-devel zlib-devel xz-devel && cd /usr/src && wget https://www.python.org/ftp/python/3.7.11/Python-3.7.11.tgz && tar xzf Python-3.7.11.tgz && cd Python-3.7.11 && ./configure --enable-optimizations && make altinstall && rm /usr/src/Python-3.7.11.tgz

RUN mkdir -p /python_api

WORKDIR /python_api

COPY python-api.py /python_api/

RUN pip3.7 install --no-cache-dir flask flask-jsonpify flask-restful

EXPOSE 5290

CMD ["python3.7", "python-api.py"]
